Readme
======
This is a data acquisition project using FPGA for the VITO experiment @ISOLDE/CERN.

Data will be acquiered continuously. For each individual anaolg input channel, data exceeding a certain trigger level between leading and traiing edge, a so called window trigger, will be collected and analyzed. Data and analysis results are sent to host for further processing.

ELVIS III is used for now. The FPGA-VI could be a starting point for the implementation using and NI-Scope with customizabel FPGA.

This project could become extended for a full AF/CS++ based control system for VITO.

Licensed under the EUPL. https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12

Copyright 2020 Helmholtzzentrum für Schwerionenforung GmbH

Author: H.Brand@gsi.de, Dr. Holger Brand, EEL, Planckstr 1, 64291 Darmstadt Germany.

LabVIEW 2019 is currently used development.

Related documents and information
=================================
- README.txt
- EUPL v.1.1 - Lizenz.pdf
- Contact: H.Brand@gsi.de
- Download, bug reports... : 
- Documentation:
  - Refer to Documantation Folder
  - Project-Wikis: 
  - NI Actor Framework: https://decibel.ni.com/content/groups/actor-framework-2011?view=overview


GIT Submodules
=================================
Following git submodules are defined in this project.

- Packages/CSPP_Core: containing the CS++ core libraries; [https://git.gsi.de/EE-LV/CSPP/CSPP_Core.git](https://git.gsi.de/EE-LV/CSPP/CSPP_Core.git)
- Packages/CSPP_ObjectManager; [https://git.gsi.de/EE-LV/CSPP/CSPP_ObjectManager.git](https://git.gsi.de/EE-LV/CSPP/CSPP_ObjectManager.git)
- Packages/CSPP_DSC: containing Alarm- & Trend-Viewer; [https://git.gsi.de/EE-LV/CSPP/CSPP_DSC.git](https://git.gsi.de/EE-LV/CSPP/CSPP_DSC.git)
- Packages/CSPP_Utilities; [https://git.gsi.de/EE-LV/CSPP/CSPP_Utilities.git](https://git.gsi.de/EE-LV/CSPP/CSPP_Utilities.git)

External Dependencies
=================================
Optional:

- Syslog; Refer to http://sine.ni.com/nips/cds/view/p/lang/de/nid/209116

Getting started:
=================================
